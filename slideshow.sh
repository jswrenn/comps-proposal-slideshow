#!/usr/bin/env bash

function slide() {
  printf "<section data-transition=\"${*:2}\">%s</section>\n" "$(cat "slides/$1.html")"
}

cat <<- _EOF_
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<title>Beyond Plain Text</title>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/reveal.js/3.3.0/css/reveal.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/reveal.js/3.3.0/css/theme/black.min.css">
	</head>
	<body>
		<div class="reveal">
			<div class="slides">
			<section data-background-iframe="assets/Title.html"></section>
			$(cat "$1"       \
				| grep . -     \
				| grep -v '^#' \
				| while read line; do
						slide $line;
					done)
			</div>
		</div>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/reveal.js/3.3.0/lib/js/head.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/reveal.js/3.3.0/js/reveal.min.js"></script>
		<script>
			// More info https://github.com/hakimel/reveal.js#configuration
			Reveal.initialize({
				history: true,
				dependencies: [
					{ src: 'lib/plugin/notes/notes.js', async: true },
				]
			});
		</script>
	</body>
</html>
_EOF_

